import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(
          title: 'Flutter Demo Home Page',
      placeTitle: 'Lake', placeDescription: 'description',),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title, required this.placeTitle, required this.placeDescription})
      : super(key: key);

  final String title;
  final String placeTitle;
  final String placeDescription;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: SafeArea(
          child: ListView(
            children: [
              imageSection(),
              titleSection(),
              buttonSection(),
              descriptionSection(),
              SizedBox(
                height: 200,
              )
            ],
          ),
        ));
  }

  Widget imageSection() {
    return Image.asset('assets/lake.png');
  }

  Widget titleSection() {
    return Padding(
      padding: const EdgeInsets.all(32.0),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  widget.placeTitle,
                  style: const TextStyle(fontSize: 18),
                ),
                const SizedBox(
                  height: 8,
                ),
                Text(
                  widget.placeDescription,
                  style: const TextStyle(fontSize: 16, color: Colors.grey),
                )
              ],
            ),
          ),
          const Icon(
            Icons.star,
            color: Colors.red,
          ),
          const Text('41')
        ],
      ),
    );
  }

  Widget buttonSection() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        buttonItem(Icons.call, 'CALL'),
        buttonItem(Icons.near_me, 'ROUTE'),
        buttonItem(Icons.share, 'SHARE')
      ],
    );
  }

  Widget buttonItem(IconData iconData, String iconDescription) {
    return Column(
      children: [
        Icon(
          iconData,
          color: Colors.lightBlue,
        ),
        Text(
          iconDescription,
          style: const TextStyle(color: Colors.lightBlue),
        ),
      ],
    );
  }

  Widget descriptionSection() {
    return Container(
      margin: const EdgeInsets.only(top: 32, left: 32, right: 32),
      child: const Text(
          'Natural lakes are generally found in mountainous areas, rift zones, and areas with ongoing glaciation. Other lakes are found in endorheic basins or along the courses of mature rivers, where a river channel has widened into a basin. Some parts of the world have many lakes formed by the chaotic drainage patterns left over from the last ice age. All lakes are temporary over long periods of time, as they will slowly fill in with sediments or spill out of the basin containing them.Natural lakes are generally found in mountainous areas, rift zones, and areas with ongoing glaciation. Other lakes are found in endorheic basins or along the courses of mature rivers, where a river channel has widened into a basin. Some parts of the world have many lakes formed by the chaotic drainage patterns left over from the last ice age. All lakes are temporary over long periods of time, as they will slowly fill in with sediments or spill out of the basin containing them.'),
    );
  }
}
